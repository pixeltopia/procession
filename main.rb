#!/usr/bin/env ruby

def process generation
    result = []
    generation.each_with_index do |one, idx|
        two = idx < generation.count - 1 ? generation[idx + 1] : nil
        case
        when one == 'b' && two == 'b'
            next
        when one == ' '
            result << 'b'
        when one == 'b'
            result << 'c'
        when one == 'c'
            result << ' '
            result << 'b'
        end
    end
    result
end

generation = [' ']

loop do
    puts generation.join
    generation = process(generation)
    sleep 0.25
end
